import { combineReducers } from 'redux';

import backend from './backend';
import user from './user';
import profileImages from './profileImages';

export default combineReducers({
  backend,
  user,
  profileImages,
});
