import R from 'ramda';

function profileImages(state = {
  currentImageSelected: null,
  uploadingNewImage: false,
  deletingImage: false,
  images: {
    one: { present: false, image: {} },
    two: { present: false, image: {} },
    three: { present: false, image: {} },
    four: { present: false, image: {} },
    five: { present: false, image: {} },
    six: { present: false, image: {} },
  },
}, action) {
  switch (action.type) {
    case 'CAPTURE_NATIVE_IMAGE_SELECT': {
      return {
        ...state,
        currentImageSelected: action.payload,
      };
    }
    case 'CAPTURE_PROFILE_IMAGES': {
      const order = R.prop('order', action.payload);
      /**
       * Automatically get the order from the image
       */
      return {
        ...state,
        images: {
          ...state.images,
          [order]: {
            present: true,
            image: action.payload,
          },
        },
      };
    }
    case 'RESET_PROFILE_IMAGES': {
      return {
        currentImageSelected: null,
        uploadingNewImage: false,
        deletingImage: false,
        images: {
          one: { present: false, image: {} },
          two: { present: false, image: {} },
          three: { present: false, image: {} },
          four: { present: false, image: {} },
          five: { present: false, image: {} },
          six: { present: false, image: {} },
        },
      };
    }
    case 'BEGIN_DELETING_IMAGE': {
      return {
        ...state,
        deletingImage: true,
      };
    }
    case 'FINISH_DELETING_IMAGE': {
      return {
        ...state,
        deletingImage: false,
        images: {
          ...state.images,
          [action.payload]: {
            present: false,
            image: {},
          },
        },
      };
    }
    case 'BEGIN_UPLOADING_USER_FILE': {
      return {
        ...state,
        uploadingNewImage: true,
      };
    }
    case 'FINISH_UPLOADING_USER_FILE': {
      return {
        ...state,
        uploadingNewImage: false,
      };
    }
    default:
      return {
        ...state,
      };
  }
}

export default profileImages;
