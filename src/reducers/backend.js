import * as firebase from 'firebase';

function backend(state = {
  firebase: {},
}, action) {
  switch (action.type) {
    case 'FIREBASE_INITIALIZED':
      return {
        firebase: firebase.initializeApp({
          apiKey: 'AIzaSyAJsNANa__RcpqCQtHoJysDnUm5BEDs0I4',
          authDomain: 'burning-inferno-2300.firebaseapp.com',
          databaseURL: 'https://burning-inferno-2300.firebaseio.com',
          storageBucket: 'burning-inferno-2300.appspot.com',
          messagingSenderId: '315037918501',
        }),
      };
    default:
      return state;
  }
}

export default backend;
