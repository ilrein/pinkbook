function user(state = {
  currentUser: {
    email: '',
  },
  geolocation: {
    discovered: false,
    reverseGeocode: null,
  },
  node: {
    loading: false,
  },
}, action) {
  switch (action.type) {
    case 'CAPTURE_USER_REGISTRATION':
      return {
        ...state,
        node: {
          ...state.node,
          ...action.payload,
        },
      };
    case 'CAPTURE_USER_AUTH_CHANGE':
      return {
        ...state,
        currentUser: action.payload,
      };
    case 'CAPTURE_USER_GEOLOCATION':
      return {
        ...state,
        geolocation: {
          discovered: true,
          ...action.payload,
        },
      };
    case 'GET_USER_NODE':
      return {
        ...state,
        node: {
          ...state.node,
          loading: false,
          ...action.payload,
        },
      };
    case 'BEGIN_UPDATING_USER_NODE':
      return {
        ...state,
        node: {
          ...state.node,
          loading: true,
        },
      };
    case 'FINISH_UPDATING_USER_NODE':
      return {
        ...state,
        node: {
          ...state.node,
          loading: false,
          ...action.payload,
        },
      };
    case 'BEGIN_UPLOADING_USER_FILE':
      return {
        ...state,
        node: { ...state.node, loading: true },
      };
    case 'FINISH_UPLOADING_USER_FILE':
      return {
        ...state,
        node: { ...state.node, loading: false },
      };
    case 'CAPTURE_NATIVE_IMAGE_SELECT':
      return {
        ...state,
        node: {
          ...state.node,
          images: action.payload,
        },
      };
    case 'RESENT_ACTIVATION_EMAIL':
    default:
      return {
        ...state,
      };
  }
}

export default user;
