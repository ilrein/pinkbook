import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'remote-redux-devtools'; // eslint-disable-line
import reducers from './';

export default function configureStore() {
  const store = createStore(
    reducers,
    composeWithDevTools(
      applyMiddleware(thunk) // eslint-disable-line
    ) // eslint-disable-line
  );
  return store;
}
