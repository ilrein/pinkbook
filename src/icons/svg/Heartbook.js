import React from "react";
import Svg, { Path } from "react-native-svg";

const Heartbook = props => (
<Svg width={props.width || 50} height={props.height || 62.5} viewBox="0 0 50 62.5">
  <Path fill="#FD8CA3" d="M45.5 47v-8h2V1h-38v38h34v8H8.4c-2.1 0-3.9-1.8-3.9-3.9 0-1.8 1.3-3.5 3-3.9V1.1c-2.8.4-5 2.8-5 5.7v36.3c.1 3.3 2.7 5.9 5.9 5.9h39.1v-2h-2zm-34-42H20v2h-6.5v6h-2V5zM20 35h-8.5v-8h2v6H20v2zm7.5-7c-6.1-2-9-7.3-9-10s2.2-5.1 4.8-5.1c1.8 0 3.3 1 4.2 2.4.8-1.4 2.4-2.4 4.2-2.4 2.7 0 4.8 2.4 4.8 5.1s-2.9 8-9 10zm16 7H35v-2h6.5v-6h2v8zm0-22h-2V7H35V5h8.5v8z"></Path></Svg>
);

export default Heartbook;
