// Kulers
export const PINK = '#FD8CA3';
export const BLACK = '#1B1C1D';

// Navbar
export const NAVBAR_SPACING = 70;

// Google Maps
export const GOOGLE_MAPS_API_KEY = 'AIzaSyCYRfCPcJC4eaP84PRDhTexOF0NA7mr5ZY';
export const GOOGLE_MAPS_REVERSE_GEOCODE_SERVICE = (lat, lng) =>
  `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${GOOGLE_MAPS_API_KEY}`;

// Mocking geolocation
export const TEST_LATITUDE = 43.8429;
export const TEST_LONGITUDE = -79.3780;
