//
// Pinkbook
//
import React, { Component } from 'react';
import { Scene, Router } from 'react-native-router-flux';
import { Provider } from 'react-redux';

// Data
import configureStore from './reducers/configureStore';

// Containers
import Home from './containers/home/';
import Login from './containers/login';
import Register from './containers/register';
import Dashboard from './containers/dashboard';
import Chat from './containers/chat';
import Profile from './containers/profile';
import Images from './containers/profile/images';
import ImageShow from './containers/profile/images/image-show';

// Step Selection
import Step2 from './ui/StepSelection/Step2';
import Step3 from './ui/StepSelection/Step3';
import Step4 from './ui/StepSelection/Step4';

// Icons
import {
  Person,
  Search,
  Chatbox,
} from './ui/TabIcon';

// Style variables
import {
  BLACK,
  // PINK,
} from './consts';

// Redux Store
const store = configureStore();

class Pinkbook extends Component {
  componentWillMount() {
    store.dispatch({
      type: 'FIREBASE_INITIALIZED',
    });
  }

  render() {
    return (
      <Provider store={store}>
        <Router>
          <Scene key="root">
            <Scene key="home" component={Home} hideNavBar initial />
            <Scene
              key="login"
              component={Login}
              title="Login"
              hideNavBar={false}
              direction="vertical"
              navigationBarStyle={{ backgroundColor: BLACK }}
              titleStyle={{ color: 'white' }}
            />
            <Scene
              key="register"
              component={Register}
              title="Register"
              hideNavBar={false}
              direction="vertical"
              navigationBarStyle={{ backgroundColor: BLACK }}
              titleStyle={{ color: 'white' }}
            />
            <Scene
              key="main"
              tabs
              tabBarStyle={{
                borderTopColor: 'white',
                borderTopWidth: 1,
                backgroundColor: BLACK,
              }}
            >
              <Scene
                key="dashboard"
                icon={Search}
                navigationBarStyle={{ backgroundColor: BLACK }}
                titleStyle={{ color: 'white' }}
                title="Dashboard"
              >
                <Scene
                  key="step1"
                  component={Dashboard}
                  renderBackButton={false}
                  title="Step 1"
                  initial
                />
                <Scene
                  key="step2"
                  component={Step2}
                  title="Step 2"
                />
                <Scene
                  key="step3"
                  component={Step3}
                  title="Step 3"
                />
                <Scene
                  key="step4"
                  component={Step4}
                  title="Step 4"
                />
              </Scene>
              <Scene
                key="chat"
                component={Chat}
                hideNavBar
                icon={Chatbox}
              />
              <Scene
                key="profile"
                icon={Person}
                navigationBarStyle={{ backgroundColor: BLACK }}
                titleStyle={{ color: 'white' }}
              >
                <Scene
                  key="profileMain"
                  component={Profile}
                  title="Profile"
                />
                <Scene
                  key="images"
                  component={Images}
                  title="Images"
                />
                <Scene
                  key="imageShow"
                  component={ImageShow}
                  title="Image"
                />
              </Scene>
            </Scene>
          </Scene>
        </Router>
      </Provider>
    );
  }
}

export default Pinkbook;
