import { StyleSheet } from 'react-native';

import {
  NAVBAR_SPACING,
  PINK,
  BLACK,
} from '../../consts';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: NAVBAR_SPACING,
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: BLACK,
  },
  loginButton: {
    marginTop: 20,
    backgroundColor: PINK,
  },
});

export default styles;
