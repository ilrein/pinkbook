//
// Login.js
//
import React, { Component, PropTypes } from 'react';
import { View, Alert, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import { Button } from 'react-native-elements';

import styles from './styles';
import Input from '../../ui/input/';

class Login extends Component {
  static propTypes = {
    firebase: PropTypes.shape({}).isRequired,
  }

  state = {
    email: '',
    password: '',
    loading: false,
  }

  onSubmit = () => {
    const { firebase } = this.props;
    const { email, password } = this.state;
    if (this.state.email === '' || this.state.password === '') {
      Alert.alert('Error', '\nNo email or password');
      return;
    }
    this.setState({
      loading: true,
    }, () => {
      firebase.auth().signInWithEmailAndPassword(
        email, password,
      )
      .catch((e) => {
        Alert.alert('Error', `\n${e.message}`);
        this.setState({ loading: false });
      })
      .then(() => {
        this.stashEmail(email);
        this.stashPassword(email);
      });
    });
  }

  handleChangeEmail = e => this.setState({ email: e });

  handleChangePassword = e => this.setState({ password: e });

  stashEmail = async () => {
    const { email } = this.state;
    try {
      await AsyncStorage.setItem('email', email);
    } catch (e) {
      console.log(e) // eslint-disable-line
    }
  }

  stashPassword = async () => {
    const { password } = this.state;
    try {
      await AsyncStorage.setItem('password', password);
    } catch (e) {
      console.log(e) // eslint-disable-line
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Input
          label="Email"
          onChangeText={this.handleChangeEmail}
        />
        <Input
          label="Password"
          password
          onChangeText={this.handleChangePassword}
        />
        {
          this.state.loading ?
            <Button
              title="Login"
              onPress={this.onSubmit}
              buttonStyle={styles.loginButton}
              loading
              borderRadius={25}
            />
          :
            <Button
              title="Login"
              onPress={this.onSubmit}
              buttonStyle={styles.loginButton}
              icon={{ name: 'send' }}
              borderRadius={25}
            />
        }
      </View>
    );
  }
}

const mapStateToProps = state => ({
  firebase: state.backend.firebase,
});

// const mapDispatchToProps = dispatch => ({
//   captureUserLogin(user) {
//     dispatch(captureUserLogin(user));
//   },
// });

export default connect(mapStateToProps)(Login);
