//
// Home.js
//
import React, { PropTypes, Component } from 'react';
import { View, Text, AsyncStorage, Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {
  Button,
} from 'react-native-elements';
import { connect } from 'react-redux';

import styles from './styles';
import Heartbook from '../../icons/svg/Heartbook';

// Actions
import captureUserLogin from '../../actions/captureUserLogin';

class Home extends Component {
  static propTypes = {
    firebase: PropTypes.shape({}).isRequired,
    captureUserLogin: PropTypes.func.isRequired,
  };

  state = {
    storage: {
      email: null,
      password: null,
    },
  }

  componentWillMount() {
    const { firebase } = this.props;
    /*
     * Capture currentUser
     */
    firebase.auth().onAuthStateChanged(
      user => this.props.captureUserLogin(user),
    );
  }

  componentDidMount() {
    const { firebase } = this.props;
    /*
     * Login the user automatically from stashed credentials
     */
    this.getCredentialsFromStash()
      .then((result) => {
        if (result.email !== null && result.password !== null) {
          firebase.auth().signInWithEmailAndPassword(
            result.email, result.password,
          )
          /*
           * handle errors with a graceful alert
           * clear the storage since the credentials failed
           * and redirect to the home container
           */
          .catch(e => Alert.alert(
            'Error!', `\n${e.message}`,
            [
              {
                text: 'OK',
                onPress: () => {
                  AsyncStorage.clear(() => {
                    Actions.home({ type: 'reset' });
                  });
                } },
            ],
          ));
        }
      });
  }

  getCredentialsFromStash = async () => {
    let email;
    let password;
    try {
      email = await AsyncStorage.getItem('email');
      password = await AsyncStorage.getItem('password');
    } catch (e) {
      console.log(e); // eslint-disable-line
    }
    return { email, password };
  }

  render() {
    return (
      <View style={styles.container}>
        <Heartbook width={100} height={125} />
        <Text style={styles.welcome}>
          Welcome to Pinkbook.
        </Text>
        <View style={styles.actions}>
          <Button
            buttonStyle={styles.login}
            raised
            icon={{ name: 'email' }}
            title="Continue with Email"
            borderRadius={25}
            onPress={Actions.login}
          />
          <Button
            buttonStyle={styles.register}
            raised
            title="Create Account"
            borderRadius={25}
            onPress={Actions.register}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  firebase: state.backend.firebase,
});

const mapDispatchToProps = dispatch => ({
  captureUserLogin(user) {
    dispatch(captureUserLogin(user));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
