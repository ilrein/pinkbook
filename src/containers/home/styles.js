import { StyleSheet } from 'react-native';

import { PINK, BLACK } from '../../consts';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 45,
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: BLACK,
  },
  welcome: {
    fontSize: 25,
    color: 'white',
  },
  actions: {
    marginTop: 10,
    marginBottom: 5,
  },
  login: {
    marginTop: 20,
    backgroundColor: PINK,
  },
  register: {
    marginTop: 20,
    backgroundColor: BLACK,

    borderTopColor: 'white',
    borderRightColor: 'white',
    borderLeftColor: 'white',
    borderBottomColor: 'white',

    borderTopWidth: 1,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderBottomWidth: 1,
  },
});

export default styles;
