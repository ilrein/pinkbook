/**
 * Dashboard
 */
import React, {
  Component,
  // PropTypes,
} from 'react';
import { connect } from 'react-redux';

import Step1 from '../../ui/StepSelection/Step1';

/* eslint-disable react/prop-types */

class DashMale extends Component {
  state = {}

  render() {
    return (
      <Step1 />
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  profileImages: state.profileImages,
});

export default connect(mapStateToProps)(DashMale);
