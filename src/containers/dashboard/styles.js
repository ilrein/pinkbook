import { StyleSheet } from 'react-native';

import {
  // NAVBAR_SPACING,
  BLACK,
} from '../../consts';

const styles = StyleSheet.create({
  container: {
    marginTop: 60,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: BLACK,
    flex: 1,
  },
});

export default styles;
