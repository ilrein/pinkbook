/**
 * Dashboard
 * Purpose: get user geolocation
 * render conditionally based on user's sex
 */
import React, { Component, PropTypes } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';

// UI
import styles from './styles';
import Dash from './dash';

// Actions
import getUserNode from '../../actions/getUserNode';
import getGeolocation from '../../actions/getGeolocation';

/* eslint-disable react/no-unused-prop-types */

class Dashboard extends Component {
  static propTypes = {
    geolocation: PropTypes.shape().isRequired,
    currentUser: PropTypes.shape({
      uid: PropTypes.string,
    }).isRequired,
    firebase: PropTypes.shape({}).isRequired,
    getGeolocation: PropTypes.func.isRequired,
    getUserNode: PropTypes.func.isRequired,
  }

  componentWillMount() {
    /**
     * get the users geolocation data
     */
    navigator.geolocation.getCurrentPosition(
      (response) => {
        this.props.getGeolocation(response);
      },
      error => console.log(error), // eslint-disable-line
    );
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentUser.uid !== undefined) {
      this.props.getUserNode(
        nextProps.currentUser.uid,
        nextProps.firebase,
      );
    }
  }

  render() {
    const { geolocation, currentUser } = this.props;
    return (
      <View style={styles.container}>
        {
          geolocation.discovered && currentUser ?
            <Dash />
             :
            <Text>Loading geolocation...</Text>
        }
      </View>
    );
  }
}

const mapStateToProps = state => ({
  firebase: state.backend.firebase,
  geolocation: state.user.geolocation,
  currentUser: state.user.currentUser,
});

const mapDispatchToProps = dispatch => ({
  getUserNode(uid, firebase) {
    dispatch(getUserNode(uid, firebase));
  },
  getGeolocation(params) {
    dispatch(getGeolocation(params));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
