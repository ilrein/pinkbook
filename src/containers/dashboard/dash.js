/**
 * Dashboard
 */
import React, {
  Component,
  PropTypes,
} from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';

import DashMale from './dash.male';
import DashFemale from './dash.female';

class Dash extends Component {
  static propTypes = {
    user: PropTypes.shape({
      node: PropTypes.shape({
        gender: PropTypes.string,
      }),
    }).isRequired,
  }

  state = {}

  render() {
    return (
      <View>
        {
          this.props.user.node.gender === 'Male' ?
            <DashMale />
          :
            null
        }
        {
          this.props.user.node.gender === 'Female' ?
            <DashFemale />
          :
            null
        }
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

export default connect(mapStateToProps)(Dash);
