import { StyleSheet } from 'react-native';

import { NAVBAR_SPACING } from '../../../../consts';

const styles = StyleSheet.create({
  container: {
    marginTop: NAVBAR_SPACING,
    height: 200,
    flex: 1,
  },
  image: {
    height: 350,
    backgroundColor: '#eee',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imagePresent: {
    height: 350,
  },
  buttonWrapper: {
    marginTop: 10,
    marginBottom: 10,
  },
  actualImage: {
    height: 350,
    width: 350,
  },
});

export default styles;
