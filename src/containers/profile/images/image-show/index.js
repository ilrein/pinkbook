//
// Image Show
//
import React, { Component, PropTypes } from 'react';
import {
  View,
  ScrollView,
  Alert,
  Text,
  Image,
} from 'react-native';
import { connect } from 'react-redux';
import CameraRollPicker from 'react-native-camera-roll-picker';
import { Button } from 'react-native-elements';

import styles from './styles';
import uploadUserFile from '../../../../actions/uploadUserFile';
import deleteUserFile from '../../../../actions/deleteUserFile';

class ImageShow extends Component {
  static propTypes = {
    uploadUserFile: PropTypes.func.isRequired,
    currentUser: PropTypes.shape({
      uid: PropTypes.string,
    }).isRequired,
    firebase: PropTypes.shape({}).isRequired,
    profileImages: PropTypes.shape({
      currentImageSelected: PropTypes.string,
      uploadingNewImage: PropTypes.bool,
      deletingImage: PropTypes.bool,
    }).isRequired,
    deleteUserFile: PropTypes.func.isRequired,
  }

  state = {
    currentSelectedImageURI: '',
  }

  componentWillReceiveProps(nextProps) {
    // console.log(nextProps.profileImages.currentImageSelected);
  }

  handleImageSelect = (imageArray) => {
    if (imageArray.length > 0) {
      this.setState({ currentSelectedImageURI: imageArray[0].uri });
    }
  }

  handleUploadImage = () => {
    if (this.state.currentSelectedImageURI === '') {
      Alert.alert('Error', 'Select an image first', [{ text: 'OK' }]);
    } else {
      this.props.uploadUserFile(
        this.props.currentUser.uid,
        this.state.currentSelectedImageURI,
        this.props.profileImages.currentImageSelected,
        this.props.firebase,
      );
    }
  };

  handleDeleteImage = () => {
    this.props.deleteUserFile(
      this.props.currentUser.uid,
      this.props.profileImages.currentImageSelected,
      this.props.firebase,
    );
  }

  render() {
    const { profileImages } = this.props;
    const currentImageIndex = profileImages.currentImageSelected;
    const URI = profileImages.images[currentImageIndex].image.uri;

    return (
      <ScrollView style={styles.container}>
        <View style={styles.image}>
          {
            URI ?
              <Image
                style={styles.actualImage}
                source={{ uri: URI }}
              />
                :
              <Text>
                IMG
              </Text>
          }
        </View>
        <View style={styles.buttonWrapper}>
          {
            URI ?
              <Button
                raised
                title="Delete"
                icon={this.props.profileImages.deletingImage ? null : { name: 'delete' }}
                onPress={this.handleDeleteImage}
                loading={this.props.profileImages.deletingImage}
              /> :
              <Button
                raised
                title="Upload IMG"
                icon={this.props.profileImages.uploadingNewImage ? null : { name: 'cloud-upload' }}
                onPress={this.handleUploadImage}
                loading={this.props.profileImages.uploadingNewImage}
              />
          }
          {/* {
            this.state.uploading ?
              <Button
                raised
                title="Upload IMG"
                onPress={this.handleUploadImage}
                loading={this.state.uploading}
              /> :
              <Button
                raised
                title="Upload IMG"
                icon={{ name: 'cloud-upload' }}
                onPress={this.handleUploadImage}
              />
          } */}
        </View>
        {
          URI ?
            null
              :
            <CameraRollPicker
              callback={this.handleImageSelect}
              maximum={1}
            />
        }
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  firebase: state.backend.firebase,
  currentUser: state.user.currentUser,
  profileImages: state.profileImages,
});

const mapDispatchToProps = dispatch => ({
  uploadUserFile(currentUserUID, imageURI, order, firebase) {
    dispatch(uploadUserFile(currentUserUID, imageURI, order, firebase));
  },
  deleteUserFile(currentUserUID, imageOrder, firebase) {
    dispatch(deleteUserFile(currentUserUID, imageOrder, firebase));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ImageShow);
