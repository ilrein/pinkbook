import React, {
  Component,
  PropTypes,
} from 'react';
import {
  ScrollView,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import { Grid, Row, Col } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
// import CameraRollPicker from 'react-native-camera-roll-picker';

import styles from './styles';
import ImagePlaceholder from '../../../ui/ImagePlaceholder';

class Images extends Component {
  static propTypes = {
    profileImages: PropTypes.shape({
      images: PropTypes.shape({}),
    }).isRequired,
  }

  state = {}

  /* eslint-disable no-console */

  handleUpload = (index) => {
    this.props.dispatch({ // eslint-disable-line
      type: 'CAPTURE_NATIVE_IMAGE_SELECT',
      payload: index,
    });
    Actions.imageShow();
  }

  render() {
    const { images } = this.props.profileImages;
    return (
      <ScrollView style={styles.container}>
        <Grid>
          <Row style={styles.topRow}>
            <Col style={styles.mainCol}>
              {
                images.one.present ?
                  <ImagePlaceholder
                    profile
                    imageIsPresent
                    uri={images.one.image.uri}
                    handlePress={() => this.handleUpload('one')}
                  /> :
                  <ImagePlaceholder
                    profile
                    imageIsPresent={false}
                    handlePress={() => this.handleUpload('one')}
                  />
              }
            </Col>
            <Col style={styles.secondCol}>
              <Row style={[styles.secondRow, styles.marginBottom]}>
                {
                  images.two.present ?
                    <ImagePlaceholder
                      imageIsPresent
                      uri={images.two.image.uri}
                      handlePress={() => this.handleUpload('two')}
                    /> :
                    <ImagePlaceholder
                      imageIsPresent={false}
                      handlePress={() => this.handleUpload('two')}
                    />
                }
              </Row>
              <Row style={styles.secondRow}>
                {
                  images.three.present ?
                    <ImagePlaceholder
                      imageIsPresent
                      uri={images.three.image.uri}
                      handlePress={() => this.handleUpload('three')}
                    /> :
                    <ImagePlaceholder
                      imageIsPresent={false}
                      handlePress={() => this.handleUpload('three')}
                    />
                }
              </Row>
            </Col>
          </Row>
        </Grid>
        <View style={styles.lastRow}>
          <View style={[styles.lastCol, styles.marginRight]}>
            {
              images.four.present ?
                <ImagePlaceholder
                  uri={images.four.image.uri}
                  handlePress={() => this.handleUpload('four')}
                  imageIsPresent
                />
                  :
                <ImagePlaceholder
                  imageIsPresent={false}
                  handlePress={() => this.handleUpload('four')}
                />
            }
          </View>
          <View style={[styles.lastCol, styles.marginRight]}>
            {
              images.five.present ?
                <ImagePlaceholder
                  uri={images.five.image.uri}
                  handlePress={() => this.handleUpload('five')}
                  imageIsPresent
                />
                  :
                <ImagePlaceholder
                  imageIsPresent={false}
                  handlePress={() => this.handleUpload('five')}
                />
            }
          </View>
          <View style={styles.lastCol}>
            {
              images.six.present ?
                <ImagePlaceholder
                  uri={images.six.image.uri}
                  handlePress={() => this.handleUpload('six')}
                  imageIsPresent
                />
                  :
                <ImagePlaceholder
                  imageIsPresent={false}
                  handlePress={() => this.handleUpload('six')}
                />
            }
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  firebase: state.backend.firebase,
  user: state.user,
  profileImages: state.profileImages,
});

// const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps)(Images);
