import { StyleSheet } from 'react-native';

import { NAVBAR_SPACING, BLACK } from '../../../consts';

const styles = StyleSheet.create({
  container: {
    paddingTop: NAVBAR_SPACING,
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: BLACK,
  },
  topRow: {
    height: 240,
    flexDirection: 'row',
  },
  mainCol: {
    flex: 0.66,
    flexDirection: 'column',
    justifyContent: 'center',
    paddingRight: 10,
  },
  secondCol: {
    flex: 0.32,
    flexDirection: 'column',
  },
  secondRow: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  lastRow: {
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'space-between',
    height: 125,
  },
  lastCol: {
    flex: 0.25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  marginBottom: {
    marginBottom: 10,
  },
  marginRight: {
    marginRight: 10,
  },
  marginLeft: {
    marginLeft: 10,
  },
  marginTop: {
    marginTop: 10,
  },
});

export default styles;
