import { StyleSheet } from 'react-native';

import { NAVBAR_SPACING, BLACK } from '../../consts';

const styles = StyleSheet.create({
  container: {
    paddingTop: NAVBAR_SPACING,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: BLACK,
  },
  wrapper: {
    marginLeft: 20,
  },
  email: { marginLeft: 20, paddingTop: 10 },
  resend: {
    marginLeft: 20,
    paddingTop: 10,
    color: 'blue',
  },
  verified: {
    marginLeft: 20,
    paddingTop: 10,
    color: 'green',
  },
  switch: {
    marginTop: 5,
    marginBottom: 5,
  },
  pictures: {
    flexDirection: 'row',
    marginTop: 20,
    marginLeft: 20,
    flex: 1,
    // paddingRight: 10,
  },
  picture: {
    // alignItems: 'flex-start',
    // alignSelf: 'center',
    // justifyContent: 'center',
  },
  firstRow: {
    marginTop: 20,
  },
  grid: {},
  row: {
    marginTop: 100,
  },
  buttonRow: {
    marginTop: 15,
  },
  spacingBottom: {
    // paddingBottom: 30,
  },
  field: {
    backgroundColor: '#fff',
    borderRadius: 5,
    paddingBottom: 15,
    marginBottom: 5,
    marginLeft: 15,
    marginRight: 15,
  },
  blue: {
    color: 'blue',
  },
  logout: {
    marginTop: 10,
    backgroundColor: BLACK,

    borderTopColor: 'white',
    borderRightColor: 'white',
    borderLeftColor: 'white',
    borderBottomColor: 'white',

    borderTopWidth: 1,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderBottomWidth: 1,
  },
});

export default styles;
