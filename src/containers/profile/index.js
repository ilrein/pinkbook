//
// Profile
//
import React, { Component, PropTypes } from 'react';
import {
  Alert,
  View,
  ScrollView,
  AsyncStorage,
  TouchableWithoutFeedback,
} from 'react-native'; // eslint-disable-line
import { Avatar, Button, FormLabel, Text } from 'react-native-elements'; // eslint-disable-line
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
// import R from 'ramda';

// UI
import { PINK, BLACK } from '../../consts';
import styles from './styles';

// Actions
import getUserNode from '../../actions/getUserNode';
import resendActivationEmail from '../../actions/resendActivationEmail';

class Profile extends Component {
  static propTypes = {
    firebase: PropTypes.shape().isRequired,
    getUserNode: PropTypes.func.isRequired,
    resendActivationEmail: PropTypes.func.isRequired,
    user: PropTypes.shape({
      node: PropTypes.shape({
        fullName: PropTypes.string,
        loading: PropTypes.bool,
        gender: PropTypes.string,
      }),
      currentUser: PropTypes.shape({
        uid: PropTypes.string,
      }),
    }),
  }

  static defaultProps = {
    user: null,
    gender: null,
    fullName: null,
  }

  componentWillMount() {
    const { firebase } = this.props;
    const uid = firebase.auth().currentUser.uid;
    this.props.getUserNode(uid, firebase);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user.node.gender !== undefined) {
      this.setState({ gender: nextProps.user.node.gender });
    }
    if (nextProps.user.node.fullName !== undefined) {
      this.setState({ fullName: nextProps.user.node.fullName });
    }
  }

  goToCameraRollView = () => Actions.images();

  resendEmail = () => {
    Alert.alert(
      'Resend Activation Email',
      'Are you sure?',
      [
        { text: 'Cancel' },
        {
          text: 'OK',
          onPress: () => this.props.resendActivationEmail(this.props.user.currentUser),
        },
      ],
    );
  };

  signOut = () => {
    const { firebase } = this.props;
    firebase.auth().signOut();
    AsyncStorage.clear(() => Actions.home({ type: 'reset' }));
  }

  render() {
    const { user } = this.props;
    return (
      user.currentUser === null ?
        null :
        <ScrollView style={styles.container}>
          <View style={styles.field}>
            <FormLabel>
              Email
            </FormLabel>
            {
              user.currentUser.email !== null ?
                <Text style={styles.email}>{user.currentUser.email}</Text> :
                null
            }
          </View>

          <View style={styles.field}>
            <FormLabel>
              Verified
            </FormLabel>
            {
              user.currentUser.emailVerified ?
                <Text style={styles.verified}>
                  True
                </Text>
              :
                <View>
                  <Text style={styles.email}>
                    False
                  </Text>

                  <TouchableWithoutFeedback
                    onPress={this.resendEmail}
                  >
                    <View style={styles.email}>
                      <Text style={styles.blue}>
                        Resend Verification Email
                      </Text>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
            }
          </View>
          <View style={styles.field}>
            <FormLabel>Full Name</FormLabel>
            <FormLabel>{user.node.fullName}</FormLabel>
          </View>
          <View style={styles.field}>
            <FormLabel>
              Gender
            </FormLabel>
            <View>
              <Text style={styles.email}>
                {user.node.gender}
              </Text>
            </View>
          </View>

          <Grid style={styles.grid}>
            <Row style={styles.buttonRow}>
              <Col>
                <Button
                  borderRadius={25}
                  title="Images"
                  icon={{ name: 'photo-library' }}
                  onPress={this.goToCameraRollView}
                  backgroundColor={PINK}
                />
              </Col>
            </Row>

            <Row style={[styles.buttonRow, styles.spacingBottom]}>
              <Col>
                <Button
                  borderRadius={25}
                  title="Logout"
                  onPress={this.signOut}
                  backgroundColor={BLACK}
                  buttonStyle={styles.logout}
                />
              </Col>
            </Row>
          </Grid>
        </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  firebase: state.backend.firebase,
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  getUserNode(uid, firebase) {
    dispatch(getUserNode(uid, firebase));
  },
  resendActivationEmail(user) {
    dispatch(resendActivationEmail(user));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
