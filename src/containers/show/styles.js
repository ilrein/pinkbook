import { StyleSheet } from 'react-native';

import { NAVBAR_SPACING } from '../../consts';

const styles = StyleSheet.create({
  container: {
    marginTop: NAVBAR_SPACING / 2,
    paddingLeft: 10,
    paddingRight: 10,
  },
});

export default styles;
