//
// Show.js
//
import React, { Component, PropTypes } from 'react';
import { Text, View } from 'react-native';

import styles from './styles';

class Show extends Component {
  static propTypes = {

  }

  state = {}

  render() {
    return (
      <View style={styles.container}>
        <Text>
          Show
        </Text>
      </View>
    );
  }
}

export default Show;
