import { StyleSheet } from 'react-native';

import { NAVBAR_SPACING, BLACK, PINK } from '../../consts';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: NAVBAR_SPACING,
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: BLACK,
  },
  centerer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  segmentedControlContainer: {
    marginTop: 30,
    width: 300,
  },
  submitButton: {
    marginTop: 30,
    backgroundColor: PINK,
  },
});

export default styles;
