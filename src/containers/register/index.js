//
// Register.js
//
import React, { Component, PropTypes } from 'react';
import {
  View,
  Alert,
  AsyncStorage,
} from 'react-native';
import { connect } from 'react-redux';
import { Button } from 'react-native-elements';
import { SegmentedControls } from 'react-native-radio-buttons';

import styles from './styles';
import { BLACK, PINK } from '../../consts';
import Input from '../../ui/input/';
import captureUserRegistration from '../../actions/captureUserRegistration';

class Register extends Component {
  static propTypes = {
    firebase: PropTypes.shape({}).isRequired, // eslint-disable-line
  }

  state = {
    loading: false,
    options: ['Male', 'Female'],
    email: '',
    password: '',
    passwordConfirmation: '',
    gender: 'Male',
    fullName: '',
  }

  onChangeEmail = email => this.setState({ email });

  onChangePassword = password => this.setState({ password });

  onChangeFullName = fullName => this.setState({ fullName });

  onChangePasswordConfirmation = passwordConfirmation => this.setState({ passwordConfirmation });

  onSubmit = async () => {
    const { firebase } = this.props;
    const { email, password, passwordConfirmation, gender, fullName } = this.state;
    if (password === passwordConfirmation) {
      if (fullName.length > 0) {
        this.setState({ loading: true }, () => {
          firebase.auth().createUserWithEmailAndPassword(
            email, password,
          )
          .catch(e => Alert.alert('Error', `\n${e.message}`))
          .then((result) => {
            /*
             * Stash crendetials in AsyncStorage
             */
            this.stashEmail();
            this.stashPassword();
            this.props.captureUserRegistration(result, gender, fullName, firebase);
          });
        });
      } else {
        Alert.alert('Error', 'Please enter your name.');
      }
    } else {
      Alert.alert('Error', 'Passwords do not match.');
    }
  }

  stashEmail = async () => {
    const { email } = this.state;
    try {
      await AsyncStorage.setItem('email', email);
    } catch (e) {
      console.log(e) // eslint-disable-line
    }
  }

  stashPassword = async () => {
    const { password } = this.state;
    try {
      await AsyncStorage.setItem('password', password);
    } catch (e) {
      console.log(e) // eslint-disable-line
    }
  }

  selectGender = gender => this.setState({ gender });

  render() {
    return (
      <View style={styles.container}>
        <Input
          label="Email"
          onChangeText={this.onChangeEmail}
        />
        <Input
          label="Password"
          password
          onChangeText={this.onChangePassword}
        />
        <Input
          label="Password Confirmation"
          password
          onChangeText={this.onChangePasswordConfirmation}
        />
        <Input
          label="Full Name"
          onChangeText={this.onChangeFullName}
        />
        <View style={styles.centerer}>
          <SegmentedControls
            options={this.state.options}
            onSelection={this.selectGender}
            containerStyle={styles.segmentedControlContainer}
            selectedOption={this.state.gender}
            tint={PINK}
            backTint={BLACK}
            selectedTint={'white'}
            selectedBackgroundColor={PINK}
            optionStyle={{ color: 'white' }}
          />
        </View>
        {this.state.loading ?
          <Button
            title="Submit"
            onPress={this.onSubmit}
            buttonStyle={styles.submitButton}
            loading
            borderRadius={25}
          /> :
          <Button
            title="Submit"
            onPress={this.onSubmit}
            buttonStyle={styles.submitButton}
            icon={{ name: 'send' }}
            borderRadius={25}
          />
        }
      </View>
    );
  }
}

const mapStateToProps = state => ({
  firebase: state.backend.firebase,
});

const mapDispatchToProps = dispatch => ({
  captureUserRegistration(user, gender, fullName, firebase) {
    dispatch(captureUserRegistration(user, gender, fullName, firebase));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
