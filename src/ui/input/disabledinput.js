//
// DisabledInput.js
//
import React, { PropTypes } from 'react';
import { View } from 'react-native';
import {
  FormLabel,
  FormInput,
} from 'react-native-elements';

const DisabledInput = ({ label }) =>
  <View>
    <FormLabel>{label}</FormLabel>
    <FormInput
      editable={false}
      defaultValue="Loading..."
    />
  </View>;

DisabledInput.propTypes = {
  label: PropTypes.string.isRequired,
};

export default DisabledInput;
