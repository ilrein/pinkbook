//
// Input.js
//
import React, { PropTypes } from 'react';
import { View } from 'react-native';
import {
  FormLabel,
  FormInput,
  FormValidationMessage,
} from 'react-native-elements';

const Input = ({ label, error, onChangeText, password, defaultValue }) =>
  <View>
    {label ?
      <FormLabel>{label}</FormLabel>
      :
      null
    }
    <FormInput
      secureTextEntry={password}
      onChangeText={onChangeText}
      autoCapitalize="none"
      defaultValue={defaultValue}
    />
    {error ? <FormValidationMessage>Error message</FormValidationMessage> : null}
  </View>;

Input.propTypes = {
  label: PropTypes.string,
  error: PropTypes.bool,
  password: PropTypes.bool,
  onChangeText: PropTypes.func.isRequired,
  defaultValue: PropTypes.string,
};

Input.defaultProps = {
  label: null,
  error: false,
  password: false,
  defaultValue: '',
};

export default Input;
