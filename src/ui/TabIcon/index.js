import React, { PropTypes } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';

import { PINK } from '../../consts';
//
// User Profile
//
export const Person = (props) => {
  const { selected } = props;
  return (
    selected
      ?
        <Icon name="ios-person" size={30} color={PINK} />
      :
        <Icon name="ios-person" size={30} color="#fff" />
  );
};

Person.propTypes = {
  selected: PropTypes.bool,
};

Person.defaultProps = {
  selected: false,
};
//
// Dashboard - Explore
//
export const Search = (props) => {
  const { selected } = props;
  return (
    selected
      ?
        <Icon name="ios-search" size={30} color={PINK} />
      :
        <Icon name="ios-search" size={30} color="#fff" />
  );
};

Search.propTypes = {
  selected: PropTypes.bool,
};

Search.defaultProps = {
  selected: false,
};
//
// Favourites
//
export const FavouritesIcon = (props) => {
  const { selected } = props;
  return (
    selected
      ?
        <Icon name="ios-heart" size={30} color={PINK} />
      :
        <Icon name="ios-heart" size={30} color="#fff" />
  );
};

FavouritesIcon.propTypes = {
  selected: PropTypes.bool,
};

FavouritesIcon.defaultProps = {
  selected: false,
};
//
// Favourites
//
export const Chatbox = (props) => {
  const { selected } = props;
  return (
    selected
      ?
        <Icon name="ios-chatboxes" size={30} color={PINK} />
      :
        <Icon name="ios-chatboxes" size={30} color="#fff" />
  );
};

Chatbox.propTypes = {
  selected: PropTypes.bool,
};

Chatbox.defaultProps = {
  selected: false,
};
