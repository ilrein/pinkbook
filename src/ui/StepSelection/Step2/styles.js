import { StyleSheet } from 'react-native';

import {
  // NAVBAR_SPACING,
  BLACK,
} from '../../../consts';

const styles = StyleSheet.create({
  container: {
    marginTop: 60,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: BLACK,
    flex: 1,
  },
  time: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    marginTop: 10,
    marginLeft: 15,
    marginRight: 15,
  },
  result: {
    paddingLeft: 10,
    paddingRight: 10,

    paddingBottom: 10,
    paddingTop: 10,

    marginTop: 10,
    marginBottom: 15,

    marginLeft: 15,
    marginRight: 15,

    backgroundColor: '#eee',
  },
});

export default styles;
