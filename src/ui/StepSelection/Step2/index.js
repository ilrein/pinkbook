import React, { Component, PropTypes } from 'react';
import { View } from 'react-native';
import { Card, Button, Text } from 'react-native-elements';
import moment from 'moment';
import { Actions } from 'react-native-router-flux';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { connect } from 'react-redux';

import styles from './styles';
import { PINK } from '../../../consts';

class Step2 extends Component {
  static propTypes = {
    profileImages: PropTypes.shape({
      images: PropTypes.shape({
        one: PropTypes.shape({
          present: PropTypes.bool,
          image: PropTypes.shape({
            uri: PropTypes.string,
          }),
        }),
      }),
    }).isRequired,
  }

  state = {
    datetime: null,
    datetimeHasBeenSet: false,
  }

  handleDatePicked = (date) => {
    const formatted = moment(date).format('MMM DD YYYY, H:mm A');
    this.setState({
      datetimeHasBeenSet: !this.state.datetimeHasBeenSet,
      datetime: formatted,
    }, () => {
      this.toggleDateTimePicker();
    });
  };

  toggleDateTimePicker = () =>
    this.setState({
      ...this.state,
      dateTimePickerVisible: !this.state.dateTimePickerVisible,
    });

  save = () => Actions.step3();

  render() {
    return (
      <View style={styles.container}>
        <DateTimePicker
          isVisible={this.state.dateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.toggleDateTimePicker}
          mode="datetime"
        />
        <Card
          title="Select Date Time"
        >
          <Button
            raised
            icon={{ name: 'date-range' }}
            backgroundColor={PINK}
            borderRadius={25}
            title="PICK DATE"
            onPress={this.toggleDateTimePicker}
          />
          <Text style={styles.time}>
            Datetime:
          </Text>
          <Text style={styles.result}>
            {this.state.datetime}
          </Text>
          <Button
            raised
            backgroundColor={PINK}
            icon={{ name: 'save' }}
            title="NEXT"
            onPress={this.save}
            borderRadius={25}
            disabled={!this.state.datetimeHasBeenSet}
          />
        </Card>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  profileImages: state.profileImages,
});

export default connect(mapStateToProps)(Step2);
