//
// Step 1 of Date Creation Wizard
//
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { View, Text } from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';
import { Card, Avatar, Button } from 'react-native-elements';
import R from 'ramda';

import styles from './styles';
import {
  PINK,
} from '../../../consts';

class Step1 extends Component {
  static propTypes = {
    profileImages: PropTypes.shape({
      images: PropTypes.shape({
        one: PropTypes.shape({
          present: PropTypes.bool,
          image: PropTypes.shape({
            uri: PropTypes.string,
          }),
        }),
      }),
    }).isRequired,
    user: PropTypes.shape({
      geolocation: PropTypes.shape({
        reverseGeocode: PropTypes.shape({
          results: PropTypes.array,
        }),
      }),
    }).isRequired,
  }

  state = {
    locality: '',
  }

  componentWillMount() {
    const { results } = this.props.user.geolocation.reverseGeocode;
    const locality = R.find(
      R.propEq('types', ['administrative_area_level_1', 'political']),
    )(results);
    this.setState({ ...this.state, locality });
  }

  handlePickTime = () => Actions.step2({ type: ActionConst.PUSH });

  handleProfileImgPress = (index) => {
    this.props.dispatch({ // eslint-disable-line
      type: 'CAPTURE_NATIVE_IMAGE_SELECT',
      payload: index,
    });
    Actions.imageShow();
  };


  render() {
    return (
      <Card
        title="Create a Date"
      >
        {
          this.props.profileImages.images.one.present ?
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Avatar
                xlarge
                rounded
                source={{ uri: this.props.profileImages.images.one.image.uri }}
                onPress={() => this.handleProfileImgPress('one')}
                activeOpacity={0.7}
                containerStyle={{ left: -70 }}
              />
            </View>
            :
            <Avatar
              xlarge
              rounded
              onPress={() => this.handleProfileImgPress('one')}
              title="IMG"
              activeOpacity={0.7}
              containerStyle={{ left: 70 }}
            />
        }
        <View style={{ marginTop: 80, marginBottom: 20 }}>
          <Text style={styles.area}>
            Setup a date in...
          </Text>
          <Text style={styles.locality}>
            {this.state.locality.formatted_address}
          </Text>
          <Button
            title="SET DATETIME"
            raised
            icon={{ name: 'navigate-next' }}
            borderRadius={25}
            backgroundColor={PINK}
            onPress={this.handlePickTime}
          />
        </View>
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
  profileImages: state.profileImages,
});

export default connect(mapStateToProps)(Step1);
