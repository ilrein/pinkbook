import { StyleSheet } from 'react-native';

import {
  NAVBAR_SPACING,
  // BLACK,
  // PINK,
} from '../../../consts';

const styles = StyleSheet.create({
  area: {
    paddingTop: NAVBAR_SPACING,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
  },
  locality: {
    backgroundColor: '#eee',
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingTop: 10,
    marginBottom: 10,
  },
});

export default styles;
