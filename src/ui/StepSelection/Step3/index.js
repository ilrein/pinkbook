//
// Date cost per hour
//
import React, { Component } from 'react';
import { View } from 'react-native';
import { Button, Card, Text, Slider } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';

import styles from './styles';
import { PINK } from '../../../consts';

class Payment extends Component {
  state = {
    spendPerHour: 10,
    firstFieldSet: false,

    totalHours: 1,
    secondFieldSet: false,

    totalCost: 10,
  }

  setFirstField = () =>
    this.setState({ ...this.state, firstFieldSet: true }, () =>
      this.calculateTotalCost());

  setSecondField = () =>
    this.setState({ ...this.state, secondFieldSet: true }, () =>
      this.calculateTotalCost());

  calculateTotalCost = () => {
    const { spendPerHour, totalHours } = this.state;
    const totalCost = spendPerHour * totalHours;
    this.setState({ ...this.state, totalCost });
  }

  handleNext = () => Actions.step4();

  render() {
    return (
      <View style={styles.container}>
        <Card title="Budget">
          <Text style={styles.text}>
            Willing to spend per hour
          </Text>
          <Slider
            value={this.state.spendPerHour}
            minimumValue={10}
            maximumValue={500}
            step={10}
            thumbTintColor={PINK}
            thumbTouchSize={{ width: 75, height: 75 }}
            onValueChange={spendPerHour => this.setState({ spendPerHour })}
            onSlidingComplete={this.setFirstField}
          />
          <Text style={styles.result}>
            ${this.state.spendPerHour}
          </Text>

          <Text style={styles.text}>
            Total hours desired
          </Text>
          <Slider
            value={this.state.totalHours}
            minimumValue={1}
            maximumValue={24}
            step={1}
            thumbTintColor={PINK}
            thumbTouchSize={{ width: 75, height: 75 }}
            onValueChange={totalHours => this.setState({ totalHours })}
            onSlidingComplete={this.setSecondField}
          />
          <Text style={styles.result}>
            {this.state.totalHours} hour(s)
          </Text>

          <Text style={styles.total}>
            Approximate cost: <Text style={styles.pink}>${this.state.totalCost}</Text>
          </Text>

          <Button
            borderRadius={25}
            raised
            backgroundColor={PINK}
            icon={{ name: 'save' }}
            title="NEXT"
            onPress={this.handleNext}
            buttonStyle={{ marginTop: 10 }}
          />
        </Card>
      </View>
    );
  }
}

export default Payment;
