import { StyleSheet } from 'react-native';

import {
  // NAVBAR_SPACING,
  BLACK,
  PINK,
} from '../../../consts';

const styles = StyleSheet.create({
  container: {
    marginTop: 60,
    paddingTop: 10,
    backgroundColor: BLACK,
    flex: 1,
  },
  text: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingTop: 10,
  },
  result: {
    backgroundColor: '#eee',
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingTop: 10,
    marginBottom: 10,
  },
  total: {
    backgroundColor: BLACK,
    color: 'white',
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingTop: 10,
    marginBottom: 10,
    fontSize: 16,
  },
  pink: {
    color: PINK,
  },
});

export default styles;
