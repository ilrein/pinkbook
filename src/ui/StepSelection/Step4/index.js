//
// Payment
//
import React, { Component } from 'react';
import { View } from 'react-native';
import { CreditCardInput } from 'react-native-credit-card-input';
import { Button } from 'react-native-elements';

import styles from './styles';
import { PINK } from '../../../consts';

class Payment extends Component {
  state = {
    cardDataIsValid: false,
  }

  handleChange = (e) => {
    if (e.valid) {
      this.setState({
        cardDataIsValid: true,
      });
    }
  }

  handleNext = () => {
    console.log('going next...'); // eslint-disable-line
  }

  render() {
    return (
      <View style={styles.container}>
        <CreditCardInput
          onChange={this.handleChange}
          color="white"
          validColor="green"
          invalidColor="red"
          labelStyle={styles.label}
          inputStyle={styles.input}
        />
        <View style={styles.next}>
          <Button
            disabled={!this.state.cardDataIsValid}
            borderRadius={25}
            raised
            backgroundColor={PINK}
            icon={{ name: 'save' }}
            title="NEXT"
            onPress={this.handleNext}
          />
        </View>
      </View>
    );
  }
}

export default Payment;
