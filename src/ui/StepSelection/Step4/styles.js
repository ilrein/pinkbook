import { StyleSheet } from 'react-native';

import {
  // NAVBAR_SPACING,
  BLACK,
} from '../../../consts';

const styles = StyleSheet.create({
  container: {
    marginTop: 60,
    paddingTop: 10,
    backgroundColor: BLACK,
    flex: 1,
  },
  label: {
    color: 'white',
  },
  input: {
    color: 'white',
  },
  next: {
    marginTop: 10,
  },
});

export default styles;
