import React, { PropTypes } from 'react';
import { View, Text, Image, TouchableWithoutFeedback } from 'react-native';

import styles from './styles';

const ImagePlaceholder = ({
  imageIsPresent,
  profile,
  uri,
  handlePress,
}) =>
  <View>
    {
      imageIsPresent ?
        <TouchableWithoutFeedback onPress={handlePress}>
          <View style={profile ? styles.profile : styles.emptyPlaceholder}>
            <Image
              style={profile ? styles.profileImage : styles.regularImage}
              source={{ uri }}
              key={uri}
            />
          </View>
        </TouchableWithoutFeedback>
      :
        <TouchableWithoutFeedback onPress={handlePress}>
          <View
            style={profile ? styles.profile : styles.emptyPlaceholder}
            onPress={handlePress}
          >
            <Text>IMG</Text>
          </View>
        </TouchableWithoutFeedback>
    }
  </View>;

ImagePlaceholder.propTypes = {
  imageIsPresent: PropTypes.bool.isRequired,
  profile: PropTypes.bool,
  handlePress: PropTypes.func.isRequired,
  uri: PropTypes.string,
};

ImagePlaceholder.defaultProps = {
  uri: '',
  profile: false,
};

export default ImagePlaceholder;
