import React, { PropTypes, Component } from 'react';
import { Avatar, Grid, Row, Col } from 'react-native-elements';
import { connect } from 'react-redux';
import R from 'ramda'; // eslint-disable-line

import styles from './styles';

// Actions
import getUserNode from '../../actions/getUserNode';
import captureProfileImages from '../../actions/captureProfileImages';

/* eslint-disable no-console */

class ImagePlaceholder extends Component {
  static propTypes = {
    user: PropTypes.shape({
      node: PropTypes.shape({
        images: PropTypes.shape({}),
      }),
    }).isRequired,
    captureProfileImages: PropTypes.func.isRequired,
  }

  state = {}

  componentWillMount() {
    /**
     * get all the user images
     * lets update the reducer
     */
    const allTheImages = this.props.user.node.images;
    console.log('all the images', allTheImages);
    /**
     * stash in the reducer
     * order as you go
     */
    R.forEachObjIndexed(
      (property) => {
        /**
         * dispatch the image
         * it will have an order
         */
        this.props.captureProfileImages(property);
      },
      allTheImages,
    );
  }

  render() {
    const { handlePress, handleDelete, profileImages } = this.props;
    console.log('profile images from render', profileImages);
    return (
      <Grid style={styles.container}>
        <Row>
          <Col style={{ flex: 0.6 }}>
            {
              profileImages.one.present ?
                <Avatar
                  onPress={() => handleDelete('one')}
                  width={240}
                  height={240}
                  source={{ uri: profileImages.one.image.uri }}
                />
              :
                <Avatar
                  title="IMG"
                  onPress={() => handlePress('one')}
                  width={240}
                  height={240}
                />
            }
          </Col>
          <Col style={{ flex: 0.5 }}>
            <Row style={{ height: 118 }}>
              {
                profileImages.two.present ?
                  <Avatar
                    onPress={() => handleDelete('two')}
                    width={118}
                    height={118}
                    containerStyle={styles.avatar}
                    source={{ uri: profileImages.two.image.uri }}
                  />
                :
                  <Avatar
                    title="IMG"
                    onPress={() => handlePress('two')}
                    width={118}
                    height={118}
                    containerStyle={styles.avatar}
                  />
              }
            </Row>
            <Row style={{ height: 118, marginTop: 4 }}>
              {
                profileImages.three.present ?
                  <Avatar
                    onPress={() => handleDelete('three')}
                    width={118}
                    height={118}
                    containerStyle={styles.avatar}
                    source={{ uri: profileImages.three.image.uri }}
                  />
                :
                  <Avatar
                    title="IMG"
                    onPress={() => handlePress('three')}
                    width={118}
                    height={118}
                    containerStyle={styles.avatar}
                  />
              }
            </Row>
          </Col>
        </Row>
        <Row style={styles.lastImageRow}>
          <Col>
            {
              profileImages.four.present ?
                <Avatar
                  onPress={() => handleDelete('four')}
                  width={118}
                  height={118}
                  source={{ uri: profileImages.four.image.uri }}
                />
              :
                <Avatar
                  title="IMG"
                  onPress={() => handlePress('four')}
                  width={118}
                  height={118}
                />
            }
          </Col>
          <Col>
            {
              profileImages.five.present ?
                <Avatar
                  onPress={() => handleDelete('five')}
                  width={118}
                  height={118}
                  containerStyle={{ marginLeft: 3 }}
                  source={{ uri: profileImages.five.image.uri }}
                />
              :
                <Avatar
                  title="IMG"
                  onPress={() => handlePress('five')}
                  width={118}
                  height={118}
                  containerStyle={{ marginLeft: 3 }}
                />
            }
          </Col>
          <Col>
            {
              profileImages.six.present ?
                <Avatar
                  onPress={() => handleDelete('six')}
                  width={118}
                  height={118}
                  containerStyle={{ marginLeft: 6 }}
                  source={{ uri: profileImages.six.image.uri }}
                />
              :
                <Avatar
                  title="IMG"
                  onPress={() => handlePress('six')}
                  width={118}
                  height={118}
                  containerStyle={{ marginLeft: 6 }}
                />
            }
          </Col>
        </Row>
      </Grid>
    );
  }
}

ImagePlaceholder.propTypes = {
  handlePress: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired,
  // user: PropTypes.shape({}).isRequired,
  profileImages: PropTypes.shape({}).isRequired,
};

const mapStateToProps = state => ({
  firebase: state.backend.firebase,
  user: state.user,
  profileImages: state.profileImages,
});

const mapDispatchToProps = dispatch => ({
  getUserNode(uid, firebase) {
    dispatch(getUserNode(uid, firebase));
  },
  captureProfileImages(images) {
    dispatch(captureProfileImages(images));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ImagePlaceholder);
