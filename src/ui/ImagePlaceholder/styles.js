import { StyleSheet } from 'react-native';

// import { BLACK } from '../../consts';

const styles = StyleSheet.create({
  profile: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',
    height: '100%',
  },
  emptyPlaceholder: {
    flexDirection: 'column',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',
    width: '100%',
    // height: 130,
  },
  profileImage: {
    height: 240,
    width: 240,
  },
  regularImage: {
    height: 115,
    width: 115,
  },
});

export default styles;
