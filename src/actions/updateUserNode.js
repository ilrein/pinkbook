export const beginUpdatingUserNode = () => ({
  type: 'BEGIN_UPDATING_USER_NODE',
});

export const updateUserNode = (uid, userData, firebase) => async (dispatch) => {
  const userRef = firebase
    .database()
    .ref(`/users/${uid}`);

  userRef.update({
    ...userData,
  });

  userRef.on('value', (snapshot) => {
    dispatch({
      type: 'FINISH_UPDATING_USER_NODE',
      payload: snapshot.val(),
    });
  });
};
