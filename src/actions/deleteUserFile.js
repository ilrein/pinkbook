import R from 'ramda';

/* eslint-disable no-console */

const deleteUserFile = (currentUserUID, imageOrder, firebase) => async (dispatch) => {
  /**
   * Begin Async Flow
   */
  dispatch({
    type: 'BEGIN_DELETING_IMAGE',
  });

  /**
   * Create a reference to
   * our user's data
   */
  const userNode = await firebase
    .database()
    .ref(`/users/${currentUserUID}`)
    .once('value')
    .then(snapshot => snapshot.val());
  /**
   * Delete the reference in the database()
   */
  R.forEachObjIndexed(
    (property, key) => {
      const imageOrderMatches = property.order === imageOrder;
      if (imageOrderMatches) {
        firebase.database()
          .ref(`/users/${currentUserUID}`)
          .child('images')
          .child(key)
          .remove()
          .then(() => {
            /**
             * Delete the item in the storage()
             */
            firebase.storage()
              .ref(`/users/${currentUserUID}`)
              .child(property.uuid)
              .delete()
              .then(() => {
                /**
                 * Let's get the updated user data
                 */
                firebase
                  .database()
                  .ref(`/users/${currentUserUID}`)
                  .once('value')
                  .then((snapshot) => {
                   /**
                    * Update the user's Node
                    */
                    dispatch({
                      type: 'GET_USER_NODE',
                      payload: snapshot.val(),
                    });
                    dispatch({
                      type: 'FINISH_DELETING_IMAGE',
                      payload: imageOrder,
                    });
                    /**
                    * Lets also map the node images
                    * to the profileImages reducer
                    */
                    const { images } = snapshot.val();
                    /**
                     * if { images } is empty, reset the profileImages
                     * reducer to its initial state
                     */
                    if (!images) {
                      dispatch({
                        type: 'RESET_PROFILE_IMAGES',
                      });
                    } else {
                      R.forEachObjIndexed(
                        (image) => {
                          dispatch({
                            type: 'CAPTURE_PROFILE_IMAGES',
                            payload: image,
                          });
                        },
                        images,
                      );
                    }
                  });
              })
              .catch(e => console.log(e.message));
          })
          .catch(e => console.log(e.message));
      }
    },
    userNode.images,
  );
};

export default deleteUserFile;
