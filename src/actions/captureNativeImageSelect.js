const captureNativeImageSelect = imageArray => ({
  type: 'CAPTURE_NATIVE_IMAGE_SELECT',
  payload: imageArray,
});

export default captureNativeImageSelect;
