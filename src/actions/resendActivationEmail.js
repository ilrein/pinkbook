const resendActivationEmail = user => async (dispatch) => {
  if (!user.emailVerified) {
    user.sendEmailVerification();
    dispatch({
      type: 'RESENT_ACTIVATION_EMAIL',
    });
  }
};

export default resendActivationEmail;
