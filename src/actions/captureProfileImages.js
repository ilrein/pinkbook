const captureProfileImages = images => ({
  type: 'CAPTURE_PROFILE_IMAGES',
  payload: images,
});

export default captureProfileImages;
