import R from 'ramda';

const getUserNode = (uid, firebase) => async (dispatch) => {
  const userNode = await firebase
    .database()
    .ref(`/users/${uid}`)
    .once('value')
    .then(snapshot => snapshot);

  /**
   * Let's map an images data in the userNode
   * to the profileImages reducer
   * for easier reasoning down the line
   */

  dispatch({
    type: 'GET_USER_NODE',
    payload: userNode.val(),
  });

  R.forEachObjIndexed(
    (image) => {
      // console.log(image);
      dispatch({
        type: 'CAPTURE_PROFILE_IMAGES',
        payload: image,
      });
    },
    userNode.val().images,
  );

  // dispatch({
  //   type:
  // })
};

export default getUserNode;
