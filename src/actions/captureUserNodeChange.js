const captureUserNodeChange = (currentUserUID, firebase) => async (dispatch) => {
  firebase
    .database()
    .ref(`/users/${currentUserUID}`)
    .on(
      'value',
      (snapshot) => {
        dispatch({
          type: 'GET_USER_NODE',
          payload: snapshot.val(),
        });
      },
    );
};

export default captureUserNodeChange;
