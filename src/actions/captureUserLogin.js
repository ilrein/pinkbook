import { Actions } from 'react-native-router-flux';

const captureUserLogin = (user) => {
  /*
   *
   * NOTE: Redirect to the dashboard whenever the user object is valid
   *
   */
  if (user !== null) Actions.main({ type: 'reset' });

  return {
    type: 'CAPTURE_USER_AUTH_CHANGE',
    payload: user,
  };
};

export default captureUserLogin;
