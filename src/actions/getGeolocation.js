import {
  GOOGLE_MAPS_REVERSE_GEOCODE_SERVICE,
  TEST_LATITUDE,
  TEST_LONGITUDE,
} from '../consts';

const URL = coords => GOOGLE_MAPS_REVERSE_GEOCODE_SERVICE(coords.latitude, coords.longitude);

const getGeolocation = (coords) => (dispatch) => { // eslint-disable-line
  fetch(URL({ latitude: TEST_LATITUDE, longitude: TEST_LONGITUDE }), {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  })
  .then(response => response.json())
  .then((json) => {
    const payload = { ...coords, reverseGeocode: json };
    dispatch({
      type: 'CAPTURE_USER_GEOLOCATION',
      payload,
    });
  });
};

export default getGeolocation;
