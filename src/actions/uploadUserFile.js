import RNFetchBlob from 'react-native-fetch-blob';
import R from 'ramda';

const uuidV4 = require('uuid/v4');

const UID = uuidV4();

const uploadUserFile = (currentUserUID, imageUri, order, firebase) => async (dispatch) => {
  dispatch({
    type: 'BEGIN_UPLOADING_USER_FILE',
  });
  /* eslint-disable no-undef */
  const wrappedImageBlobFromUri = RNFetchBlob.wrap(imageUri);
  const Blob = RNFetchBlob.polyfill.Blob;
  window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
  window.Blob = Blob;
  /* eslint-enable */
  Blob
    .build(wrappedImageBlobFromUri, { type: 'image/jpg' })
    .then((blob) => {
      firebase.storage()
        .ref(`users/${currentUserUID}`)
        .child(UID)
        .put(blob, { contentType: 'image/jpg' })
        .then((snapshot) => {
          const uri = snapshot.a.downloadURLs[0];

          const userRef = firebase
            .database()
            .ref(`/users/${currentUserUID}`)
            .child('images');

          userRef.push().update({
            uri,
            order,
            uuid: UID,
          });

          dispatch({
            type: 'FINISH_UPLOADING_USER_FILE',
          });

          firebase
            .database()
            .ref(`/users/${currentUserUID}`)
            .once('value')
            .then((userNode) => {
              /**
               * Update the user's Node
               */
              dispatch({
                type: 'GET_USER_NODE',
                payload: userNode.val(),
              });
              /**
               * Lets also map the node images
               * to the profileImages reducer
               */
              const { images } = userNode.val();
              R.forEachObjIndexed(
                (image) => {
                  dispatch({
                    type: 'CAPTURE_PROFILE_IMAGES',
                    payload: image,
                  });
                },
                images,
              );
            });
        });
    });
};

export default uploadUserFile;
