import { Actions } from 'react-native-router-flux';

const captureUserRegistration = (user, gender, fullName, firebase) => async (dispatch) => {
  /*
   *
   * NOTE: Redirect to the dashboard whenever the user object is valid
   *
   */
  if (user !== null) {
    Actions.main({ type: 'reset' });
    /*
     * Let's send a SINGLE email verification
     */
    if (!user.emailVerified) user.sendEmailVerification();
    /*
     * Update the user's node onRegister
     */
    const userRef = firebase
      .database()
      .ref(`/users/${user.uid}`);

    userRef.update({
      gender,
      fullName,
      // creditCard:
    });

    userRef.on('value', (snapshot) => {
      dispatch({
        type: 'CAPTURE_USER_REGISTRATION',
        payload: snapshot.val(),
      });
    });
  }
};

export default captureUserRegistration;
