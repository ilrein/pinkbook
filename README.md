# Pinkbook
  - Application Flow
    - Home Screen (Unauthenticated)
      - Login
      - Register
    - Dashboard (Authenticated)
      - Get currentUser geolocation

### Creating JS files from SVG
  - msvgc -f ./src/icons/heartbook.svg --react-native -o ./src/icons
  - [MSVGC](https://github.com/necinc/msvgc)

### Marilyn Monroe Color Theme
  - [Adobe Kuler](https://color.adobe.com/Marilyn-Monroe-color-theme-7488194/edit/?copy=true)

### Icons
  - Book by Dinosoft Labs from the Noun Project
